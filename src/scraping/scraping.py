import urllib.request
import csv
from bs4 import BeautifulSoup

urlpage = 'http://www.fasttrack.co.uk/league-tables/tech-track-100/league-table/'
page = urllib.request.urlopen(urlpage)

soup = BeautifulSoup(page, 'html.parser')
print(soup)

table = soup.find('table', attrs={'class': 'tableSorter'})
results = table.find_all('tr')
print('Number of results', len(results))

rows = []
rows.append(['Rank', 'Company Name', 'Webpage', 'Description', 'Location', 'Year end', 'Annual sales rise over 3 years', 'Sales £000s', 'Staff', 'Comments'])
print(rows)

for result in results:
    # find all columns per result
    data = result.find_all('td')
    # check that columns have data 
    if len(data) == 0: 
        continue

rank = data[0].getText()
company = data[1].getText()
location = data[2].getText()
yearend = data[3].getText()
salesrise = data[4].getText()
sales = data[5].getText()
staff = data[6].getText()
comments = data[7].getText()

print('Company is', company)
# Company is WonderblyPersonalised children's books          
print('Sales', sales)
# Sales *25,860

# extract description from the name
companyname = data[1].find('span', attrs={'class':'company-name'}).getText()    
description = company.replace(companyname, '')

# remove unwanted characters
sales = sales.strip('*').strip('†').replace(',','')

# go to link and extract company website
url = data[1].find('a').get('href')
page = urllib.request.urlopen(url)
# parse the html 
soup = BeautifulSoup(page, 'html.parser')
# find the last result in the table and get the link
try:
    tableRow = soup.find('table').find_all('tr')[-1]
    webpage = tableRow.find('a').get('href')
except:
    webpage = None