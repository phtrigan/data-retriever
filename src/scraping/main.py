# import libraries
from flask import Flask
from flask_restful import Resource, Api

app = Flask(__name__)

@app.route('/')

def hello_world:
    return "Hey, we have Flask in a Docker container!"


if __name == '__main__':
    app.run(debug=true, host='0.0.0.0')