[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/phtrigan%2Fdata-retriever/3b7e045aa59d26422b80ce97d243a2ba957093f7)
.. image:: https://mybinder.org/badge.svg :target: https://mybinder.org/v2/gl/phtrigan%2Fdata-retriever/3b7e045aa59d26422b80ce97d243a2ba957093f7
# Data retriever

Scrap data from the web to store it in a structured way.
