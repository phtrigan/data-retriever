FROM php:7.2-cli
CMD ["php", "-S", "localhost:8000", "src/index/php"]
COPY src/ /var/www/html
EXPOSE 80